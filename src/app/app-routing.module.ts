import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DietViewComponent } from './components/diet-view/diet-view.component';
import { TrainingViewComponent } from './components/training-view/training-view.component';
import { RecipesContainerComponent } from './components/diet-view/recipes-container/recipes-container.component';
import { ProductsContainerComponent } from './components/diet-view/products-container/products-container.component';
import { DiaryContainerComponent } from './components/diet-view/diary-container/diary-container.component';

const routes: Routes = [
  { path: '', redirectTo: '/diet/diary', pathMatch: 'full' },
  {
    path: 'diet', component: DietViewComponent,
    children: [
      { path: '', redirectTo: 'diary', pathMatch: 'full' },
      { path: 'diary', component: DiaryContainerComponent },
      { path: 'recipes', component: RecipesContainerComponent },
      { path: 'products', component: ProductsContainerComponent }

    ]
  },
  { path: 'training', component: TrainingViewComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
