import { Product } from '../models/product.model';
import { Injectable } from '@angular/core';

@Injectable({providedIn: 'root'})
export class ProductsService {
  products: Product[] = [
    {
    id:1,
    name: 'products 1',
    calories: 200,
    carbs: 20,
    protein: 30,
    fat: 50
  },
  {
    id:1,
    name: 'products 2',
    calories: 3300,
    carbs: 132,
    protein: 300,
    fat: 3
  },    {
    id:1,
    name: 'products 3',
    calories: 2000,
    carbs:4350,
    protein: 2340,
    fat: 342
  },
];
  contructor(){}

  getAllProducts () {
    return this.products;
  }
}