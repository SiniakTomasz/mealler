export interface Product {
  id: number;
  name: string;
  carbs: number;
  protein: number;
  fat: number;
  calories: number
}