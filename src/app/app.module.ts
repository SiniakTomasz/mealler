import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { DietViewComponent } from './components/diet-view/diet-view.component';
import { TrainingViewComponent} from './components/training-view/training-view.component';
import { ProductsContainerComponent } from './components/diet-view/products-container/products-container.component';
import { DiaryContainerComponent } from './components/diet-view/diary-container/diary-container.component';
import { RecipesContainerComponent } from './components/diet-view/recipes-container/recipes-container.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DietViewComponent,
    TrainingViewComponent,
    RecipesContainerComponent,
    ProductsContainerComponent,
    DiaryContainerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
