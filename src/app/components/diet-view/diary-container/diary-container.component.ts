import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-diary-container',
  templateUrl: './diary-container.component.html',
  styleUrls: ['./diary-container.component.scss']
})
export class DiaryContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
